{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module Server.Domain.Config where

import Data.Aeson      (FromJSON, ToJSON, eitherDecode)
import Data.ByteString (ByteString)
import Data.Text       (Text, pack)
import GHC.Generics    (Generic)

import qualified Data.ByteString.Lazy as BSL (ByteString, pack, readFile)


data AppConfigFile = AppConfigFile {
    app :: AppConfig
} deriving (Show, Generic)

data AuthConfigFile = AuthConfigFile {
    aws        :: Maybe AwsConfig
    -- credential :: Maybe CredentialConfig,
    -- database   :: Maybe DatabaseConfig
} deriving (Show, Generic)


data AppConfig = AppConfig {
   clientUrl :: String,
   serverUrl :: String
} deriving (Show, Generic)

data AwsConfig = AwsConfig {
   key      :: String,
   secret   :: String,
   session  :: Maybe String,
   sender   :: String,
   to       :: [String],
   region   :: Text
} deriving (Show, Generic)

instance FromJSON AppConfigFile
instance ToJSON AppConfigFile

instance FromJSON AuthConfigFile
instance ToJSON AuthConfigFile

instance FromJSON AppConfig
instance ToJSON AppConfig

instance FromJSON AwsConfig
instance ToJSON AwsConfig


readAuth :: IO AwsConfig
readAuth = do
   file <- BSL.readFile "auth.json"
   let eitherFile = eitherDecode file :: Either String AwsConfig
   return $ unwrap eitherFile

unwrap :: Either a b -> b
unwrap (Left _) = error "Error decoding ByteString into JSON"
unwrap (Right x) = x
