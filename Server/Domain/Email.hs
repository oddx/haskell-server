{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module Server.Domain.Email where

import Control.Monad.Trans     (liftIO)
import Data.Aeson              (FromJSON, ToJSON)
import Data.ByteString         (ByteString)
import Data.Text.Lazy          (Text, append, pack)
import Data.Text.Lazy.Encoding (encodeUtf8)
import GHC.Generics            (Generic)
import Network.HTTP.Client     (newManager)
import Network.HTTP.Client.TLS (tlsManagerSettings)
import Network.Mail.Mime       (simpleMail', Address(Address), plainPart)
import Network.Mail.Mime.SES   (SES(SES), renderSendMailSES)
import Server.Domain.Config    (AwsConfig, key, secret, sender, to, region)
import Web.Scotty              (ActionM)

import qualified Data.ByteString.Lazy as BSL (ByteString, toStrict)
import qualified Data.Text            as T   (pack)



data Email = Email 
   { name :: Text
   , address :: Text
   , services :: Text
   } deriving (Show, Generic)

instance FromJSON Email where
instance ToJSON Email where

sendEmail :: Email -> AwsConfig -> ActionM ()
sendEmail x y = do
   manager <- liftIO (newManager tlsManagerSettings)
   let content         = (name x) `append` (pack " would like services ") `append` 
                         (services x) `append` (pack " and can be reached at ") `append` 
                         (address x)
       sesFrom         = toStrictByteString . sender $ y
       sesAccessKey    = toStrictByteString . key $ y
       sesSecretKey    = toStrictByteString . secret $ y
       sesSessionToken = Nothing
       sesRegion       = region y
       sesTo           = fmap toStrictByteString . to $ y
       ses             = SES sesFrom sesTo sesAccessKey sesSecretKey sesSessionToken sesRegion
       mail            = simpleMail'
                         (Address Nothing (T.pack . sender $ y))
                         (Address Nothing (head . fmap T.pack . to $ y))
                         (T.pack "Potential Business")
                         content
   renderSendMailSES manager ses mail

toStrictByteString :: String -> ByteString
toStrictByteString x = BSL.toStrict . encodeUtf8 . pack $ x
