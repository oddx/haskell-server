{-# LANGUAGE OverloadedStrings #-}

import Server.Domain.Email  (Email, sendEmail)
import Server.Domain.Config (AwsConfig, readAuth)

import Control.Monad.Trans                  (liftIO)
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Web.Scotty                           (ActionM, get, jsonData, middleware, post, scotty, text)

main = scotty 3000 $ do
   middleware $ logStdoutDev
   
   get "/api/health" $ do
      text "I am healthy!\n"

   post "/api/contact" $ do
      email <- jsonData :: ActionM Email
      (liftIO $ readAuth) >>= sendEmail email
